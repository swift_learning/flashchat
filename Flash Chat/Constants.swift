//
//  Constants.swift
//  Flash Chat
//
//  Created by iulian david on 7/29/17.
//  Copyright © 2017 London App Brewery. All rights reserved.
//

import Foundation

struct Segues {
    static let loginSegue = "goToLogin"
    static let chatSegue = "goToChat"
    static let registrationSegue = "goToRegistration"
    
}

struct Tables {
    static let cellIdentifier = "MessageCell"
}
