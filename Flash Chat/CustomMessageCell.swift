//
//  CustomMessageCell.swift
//  Flash Chat
//
//  Created by Angela Yu on 30/08/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit
import Firebase
import ChameleonFramework

class CustomMessageCell: UITableViewCell {


    @IBOutlet var messageBackground: UIView!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var messageBody: UILabel!
    @IBOutlet var senderUsername: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code goes here
    }

    func configureCell(message: String? = "" , sender: String? = "") {
        if let message = message {
            messageBody.text = message
        }
        
        if let sender = sender {
            senderUsername.text = sender
        }
        avatarImageView.image = #imageLiteral(resourceName: "egg")
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.height / 2
        avatarImageView.clipsToBounds = true
        
        if sender == Auth.auth().currentUser?.email {
            avatarImageView.backgroundColor = .flatMint()
            messageBackground.backgroundColor = .flatSkyBlue()
            messageBackground.transform = .identity
            avatarImageView.transform = .identity
        } else {
            messageBackground.backgroundColor = UIColor.flatGray()
            avatarImageView.backgroundColor = .flatWatermelon()
            
            // set image to right when the message it's not from current user
            let avatarWidth = avatarImageView.bounds.width
            messageBackground.transform = CGAffineTransform(translationX: -avatarWidth, y: 0)
            let screenWidth = UIScreen.main.bounds.width
            avatarImageView.transform = CGAffineTransform(translationX: screenWidth - avatarWidth - 9, y: 0)
            
        }
    }
}
