//
//  ViewController.swift
//  Flash Chat
//
//  Created by Angela Yu on 29/08/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit
import Firebase

class ChatViewController: UIViewController {
    
    // Declare instance variables here
    var messageArray: [Message] = []
    
    // We've pre-linked the IBOutlets
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var messageTextfield: UITextField!
    @IBOutlet var messageTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the table delegate and datasource
        messageTableView.delegate = self
        messageTableView.dataSource = self
        messageTableView.reloadData()
        
        //Set the tapGesture here:
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
        messageTableView.addGestureRecognizer(tapGesture)
        

        //TODO: Register MessageCell.xib:
        messageTableView.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: Tables.cellIdentifier)
        
        configureTableView()
        retrieveMessage()
        
        messageTableView.separatorStyle = .none
        //flips the tableview (and all cells) upside down
        messageTableView.transform = CGAffineTransform.init(scaleX: 1, y: -1)
    }

    //MARK: Register for keyboard appearance notifications
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    
    func configureTableView() {
        messageTableView.rowHeight = UITableViewAutomaticDimension
        messageTableView.estimatedRowHeight = 120.0
    }
    

    
    ///////////////////////////////////////////
    
    
    //MARK: - Send & Recieve from Firebase
    fileprivate func outputMessage(_ message: Message) {
       
            self.messageArray.insert(message, at: 0)
            self.configureTableView()
       
            self.messageTableView.beginUpdates()
            let index = IndexPath(row: 0, section: 0)
            self.messageTableView.insertRows(at: [index], with: .left)
            self.messageTableView.endUpdates()

    }
    
    func retrieveMessage() {
        let messagesDB = Database.database().reference().child(Message.dbReference)
        messagesDB.observe(.childAdded) { (snapshot) in
            if let snapshotValue = snapshot.value as? Dictionary<String, String> {
                let message = Message(from: snapshotValue )
                self.outputMessage(message)
                
            }
        }
    }
    
    
    
    
    @IBAction func sendPressed(_ sender: AnyObject) {
        
        messageTextfield.endEditing(true)
        
        //TODO: Send the message to Firebase and save it in our database
        messageTextfield.isEnabled = false
        sendButton.isEnabled = false
        
        let messagesDB = Database.database().reference().child(Message.dbReference)
        
        let messageDictionary = Message.getDictionaryForm(message: messageTextfield.text!, email: (Auth.auth().currentUser?.email)!)
        
        messagesDB.childByAutoId().setValue(messageDictionary) { error, reference in
            if error != nil {
                print(error?.localizedDescription ?? "Unknow error")
            } else {
                print("Message saved successfully")
                self.messageTextfield.isEnabled = true
                self.sendButton.isEnabled = true
                self.messageTextfield.text = ""
            }
        }
        
    }
    
    
    @IBAction func logOutPressed(_ sender: AnyObject) {
        
        //TODO: Log out the user and send them back to WelcomeViewController
        try? Auth.auth().signOut()
        guard (navigationController?.popToRootViewController(animated: true)) != nil else {
            return
        }
        
    }

    
    @objc func tableViewTapped() {
        messageTextfield.endEditing(true)
    }
}

//MARK: - TableView DataSource Methods
extension ChatViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = messageTableView.dequeueReusableCell(withIdentifier: Tables.cellIdentifier, for: indexPath)
            as? CustomMessageCell  else {
                return UITableViewCell()
        }

        let message = messageArray[indexPath.row]
        cell.configureCell(message: message.messageBody, sender: message.sender)
        //flips the cell to appear oriented correctly
        cell.transform = CGAffineTransform.init(scaleX: 1, y: -1)
        return cell
    }
    
}


extension ChatViewController: UITableViewDelegate {
    
}


//MARK:- Keyboard Show And Hide
extension ChatViewController {
    
    
    //keyboard related
    @objc func keyboardWillShow(notification: NSNotification) {
        
        let userInfo = notification.userInfo as! [String: NSObject] as NSDictionary
        let keyboardFrame = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! CGRect
        let keyboardHeight = keyboardFrame.height
        heightConstraint.constant = keyboardHeight + 50
       
        UIView.animate(withDuration: 0.3) {
              self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        heightConstraint.constant = 50.0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}
