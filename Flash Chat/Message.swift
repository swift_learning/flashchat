//
//  Message.swift
//  Flash Chat
//
//  This is the model class that represents the blueprint for a message

class Message {
    
    var sender: String = ""
    var messageBody: String = ""
 
    static let dbReference = "Messages"
    
    static let messageText = "MessageBody"
    static let senderText = "Sender"
    
    convenience init(from dictionary: Dictionary<String, String>) {
        self.init()
        if let sender = dictionary[Message.senderText] {
            self.sender = sender
        }
        if let messageBody = dictionary[Message.messageText] {
            self.messageBody = messageBody
        }
    }
    public static func getDictionaryForm(message: String, email: String) -> Dictionary<String, String> {
        return [senderText : email, messageText : message]
    }
}
